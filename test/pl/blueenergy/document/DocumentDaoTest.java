package pl.blueenergy.document;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DocumentDaoTest {

    @Test
    void assertTrueIfSizeOfListFromDatabaseHaveAnyElements() {
        DocumentDao documentDao = new DocumentDao();
        int size = documentDao.getAllDocumentsInDatabase().size();
        assertTrue(size > 0);
    }

    @Test
    void assertTrueIfDatabaseIsntNull() {
        DocumentDao documentDao = new DocumentDao();
        assertNotNull(documentDao);
    }

}