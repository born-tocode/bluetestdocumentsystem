import pl.blueenergy.document.ApplicationForHolidays;
import pl.blueenergy.document.DocumentDao;
import pl.blueenergy.document.Questionnaire;
import pl.blueenergy.organization.User;
import pl.blueenergy.service.*;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class ProgrammerService {

    private final SelectorOfQuestionnaire SELECTOR_OF_QUESTIONNAIRE;
    private final DocumentSplitter DOCUMENT_SPLITTER;
    private final AnswersCounter ANSWERS_COUNTER;
    private final UsernameValidator HOLIDAYS_USERS;
    private final DateValidator DATE_VALIDATOR;
    private final OutBuffer OUT_BUFFER;
    private final SalaryChanger SALARY_CHANGER;

    public ProgrammerService() {
        this.SELECTOR_OF_QUESTIONNAIRE = new SelectorOfQuestionnaire();
        this.DOCUMENT_SPLITTER = new DocumentSplitter();
        this.ANSWERS_COUNTER = new AnswersCounter();
        this.HOLIDAYS_USERS = new UsernameValidator();
        this.DATE_VALIDATOR = new DateValidator();
        this.OUT_BUFFER = new OutBuffer();
        this.SALARY_CHANGER = new SalaryChanger();
    }

    public void execute(DocumentDao documentDao) throws NoSuchFieldException, IllegalAccessException {
        readEvaluatePrintLoop(documentDao);
    }

    public void readEvaluatePrintLoop(DocumentDao documentDao) throws NoSuchFieldException, IllegalAccessException {
        Questionnaire singleQuestionnaire;
        List<Questionnaire> questionnaires;
        List<ApplicationForHolidays> applicationForHolidays;
        List<User> usersForHolidays;
        List<String> invalidUsernames;
        List<String> invalidDateUsers;
        User singleUser;

        DOCUMENT_SPLITTER.splitDocuments(documentDao);
        questionnaires = DOCUMENT_SPLITTER.getQuestionnaires();
        applicationForHolidays = DOCUMENT_SPLITTER.getApplicationForHolidays();
        usersForHolidays = HOLIDAYS_USERS.getUsersFromApplications(applicationForHolidays);
        invalidUsernames = HOLIDAYS_USERS.validateUsername(usersForHolidays);
        invalidDateUsers = DATE_VALIDATOR.validateDate(applicationForHolidays);

        Scanner scanner = new Scanner(System.in);

        printInfo();

        try {
            while (scanner.hasNextInt()) {
                switch (scanner.nextInt()) {
                    case 1:
                        int numberOfQuestionnaires = DOCUMENT_SPLITTER.getQuestionnaires().size();
                        double averageAnswersOnQuestion = ANSWERS_COUNTER.countAverageAnswers(questionnaires);
                        int numberOfApplicationForHolidays = DOCUMENT_SPLITTER.getApplicationForHolidays().size();

                        System.out.println();
                        System.out.println("Average answers on question is: " + (int) averageAnswersOnQuestion);

                        System.out.println();
                        System.out.println("List of users who applicable for holidays:");
                        if (usersForHolidays.isEmpty()) System.out.println("No one applied for holidays");
                        usersForHolidays.forEach(user -> System.out.println(user.getLogin()));

                        System.out.println();
                        System.out.println("List of invalid username:");
                        if (invalidUsernames.isEmpty()) System.out.println("All users have valid username");
                        invalidUsernames.forEach(System.out::println);

                        System.out.println();
                        System.out.println("Username from application with invalid date:");
                        if (invalidDateUsers.isEmpty()) System.out.println("All users set properly date");
                        invalidDateUsers.forEach(System.out::println);

                        System.out.println();
                        System.out.println("All questionnaires in system: " + numberOfQuestionnaires);

                        System.out.println();
                        System.out.println("All application for holidays: " + numberOfApplicationForHolidays);

                        printInfo();
                        break;
                    case 2:
                        System.out.println("Choose a questionnaire for save in file:");
                        SELECTOR_OF_QUESTIONNAIRE.printAllQuestionnaires(questionnaires);
                        singleQuestionnaire = SELECTOR_OF_QUESTIONNAIRE.allowUserChooseQuestionnaire(questionnaires);
                        OUT_BUFFER.saveQuestionnairesToFile(singleQuestionnaire);
                        OUT_BUFFER.printInfoAfterSaving();
                        printInfo();
                        break;
                    case 3:
                        System.out.println("Please choose user for set new salary:");
                        singleUser = SALARY_CHANGER.allowUserGetUser(usersForHolidays);
                        SALARY_CHANGER.setSalary(singleUser);
                        printInfo();
                        break;
                    case 4:
                        scanner.close();
                        System.exit(1);
                    default:
                        System.out.println("Bad choice. Try again");
                        printInfo();
                        break;
                }
            }
        } catch (InputMismatchException e) {
            throw new InputMismatchException("Invalid input data");
        }
    }

    private void printInfo() {
        System.out.println();
        System.out.println("--------");
        System.out.println("1. Print info about all documents in system");
        System.out.println("2. Save questionnaires to file");
        System.out.println("3. Change salary");
        System.out.println("4. Exit");
        System.out.println("--------");
        System.out.println();
    }
}