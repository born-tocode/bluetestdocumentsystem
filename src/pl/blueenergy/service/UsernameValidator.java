package pl.blueenergy.service;

import pl.blueenergy.document.ApplicationForHolidays;
import pl.blueenergy.organization.User;

import java.util.LinkedList;
import java.util.List;

public class UsernameValidator {

    private List<User> userList;

    public List<User> getUsersFromApplications(List<ApplicationForHolidays> applicationForHolidays) {
        userList = new LinkedList<>();
        applicationForHolidays.forEach(application -> userList.add(application.getUserWhoRequestAboutHolidays()));

        return userList;
    }

    public List<String> validateUsername(List<User> users) {
        List<String> invalidUsername = new LinkedList<>();

        users
                .forEach(user -> {
                    if (!user.getLogin().matches("(^[\\w.-]{0,19}[0-9a-zA-Z]$)"))
                        invalidUsername.add(user.getLogin());
                });

        return  invalidUsername;
    }
}
