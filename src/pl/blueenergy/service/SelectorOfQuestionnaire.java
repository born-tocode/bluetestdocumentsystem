package pl.blueenergy.service;

import pl.blueenergy.document.Questionnaire;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class SelectorOfQuestionnaire {

    private final Scanner SCANNER = new Scanner(System.in);
    private int count = 0;

    public void printAllQuestionnaires(List<Questionnaire> questionnaires) {

        questionnaires.forEach(questionnaire -> {
            System.out.println(count + ". " + questionnaire.getTitle());
            count++;
        });

        count = 0;
    }

    public Questionnaire allowUserChooseQuestionnaire(List<Questionnaire> questionnaireList) {
        Questionnaire questionnaire = new Questionnaire();

        try {
            if (SCANNER.hasNextInt())
                questionnaire = questionnaireList.get(SCANNER.nextInt());
        } catch (InputMismatchException e) {
            throw new InputMismatchException("Bad choice");
        }

        return questionnaire;
    }
}
