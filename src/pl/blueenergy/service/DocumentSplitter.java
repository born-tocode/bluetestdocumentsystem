package pl.blueenergy.service;

import pl.blueenergy.document.ApplicationForHolidays;
import pl.blueenergy.document.DocumentDao;
import pl.blueenergy.document.Questionnaire;

import java.util.LinkedList;
import java.util.List;

public class DocumentSplitter {

    private List<ApplicationForHolidays> applicationForHolidays;
    private List<Questionnaire> questionnaires;

    public void splitDocuments(DocumentDao documentDao) {
        applicationForHolidays = new LinkedList<>();
        questionnaires = new LinkedList<>();

        documentDao
                .getAllDocumentsInDatabase()
                .forEach(document -> {

                    if (document instanceof Questionnaire)
                        questionnaires.add((Questionnaire) document);

                    if (document instanceof ApplicationForHolidays)
                        applicationForHolidays.add((ApplicationForHolidays) document);

        });
    }

    public List<ApplicationForHolidays> getApplicationForHolidays() {
        return applicationForHolidays;
    }

    public List<Questionnaire> getQuestionnaires() {
        return questionnaires;
    }
}
