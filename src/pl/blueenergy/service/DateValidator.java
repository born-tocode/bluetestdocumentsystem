package pl.blueenergy.service;

import pl.blueenergy.document.ApplicationForHolidays;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class DateValidator {

    private List<String> usersWithInvalidDate;

    public List<String> validateDate(List<ApplicationForHolidays> applications) {
        usersWithInvalidDate = new LinkedList<>();

        applications.forEach(application -> {
            Date since = application.getSince();
            Date to = application.getTo();

            if (since.compareTo(to) > 0)
                usersWithInvalidDate.add(application.getUserWhoRequestAboutHolidays().getLogin());
        });

        return usersWithInvalidDate;
    }
}
