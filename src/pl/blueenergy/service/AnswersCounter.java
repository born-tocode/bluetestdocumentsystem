package pl.blueenergy.service;

import pl.blueenergy.document.Questionnaire;

import java.util.List;

public class AnswersCounter {

    private double numberOfAllQuestions;
    private double numberOfAllAnswers;

    public double countAverageAnswers(List<Questionnaire> questionnaires) {
        double averageOfAllAnswers;

        questionnaires.forEach(questionnaire -> {
            numberOfAllQuestions += questionnaire.getQuestions().size();

            questionnaire
                    .getQuestions()
                    .forEach(question -> numberOfAllAnswers += question.getPossibleAnswers().size());
        });

        averageOfAllAnswers = Math.round(numberOfAllAnswers / numberOfAllQuestions);

        return averageOfAllAnswers;
    }
}
