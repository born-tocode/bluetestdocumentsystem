package pl.blueenergy.service;

import pl.blueenergy.document.Question;
import pl.blueenergy.document.Questionnaire;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class OutBuffer {

    private final String FILE_NAME = "AllQuestionnaires.txt";

    public void saveQuestionnairesToFile(Questionnaire questionnaire) {
        int countQuestion = 0;
        int countAnswers = 1;

        for (Question question : questionnaire.getQuestions()) {
            List<String> possibleAnswers = question.getPossibleAnswers();

            try (PrintWriter outputStream = new PrintWriter(new FileWriter(FILE_NAME, true))) {

                outputStream.println();
                if (countQuestion == 0) {
                    outputStream.print(questionnaire.getTitle());
                    outputStream.println();
                    outputStream.println();
                }

                outputStream.print(question.getQuestionText());
                outputStream.println();

                for (String answer : possibleAnswers) {
                    outputStream.printf("%7s", countAnswers + ". ");
                    outputStream.print(answer);
                    outputStream.write("\n");

                    countAnswers++;
                }

                countAnswers = 1;
            } catch (IOException e) {
                e.printStackTrace();
            }
            countQuestion++;
        }
    }

    public void printInfoAfterSaving() {
        System.out.println("Questionnaire was saved in " + FILE_NAME);
    }
}
