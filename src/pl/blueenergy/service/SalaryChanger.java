package pl.blueenergy.service;

import pl.blueenergy.organization.User;

import java.lang.reflect.Field;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class SalaryChanger {

    private final Scanner SCANNER = new Scanner(System.in);
    private int count = 0;

    public User allowUserGetUser(List<User> usersForHolidays) {
        User singleUser = null;

        usersForHolidays.forEach(user -> {
            System.out.println(count + ". " + user.getLogin());
            count++;
        });

        try {
            if (SCANNER.hasNextInt())
                singleUser = usersForHolidays.get(SCANNER.nextInt());
        } catch (InputMismatchException e) {
            throw new InputMismatchException("Bad choice");
        }

        return singleUser;
    }

    public void setSalary(User user) throws NoSuchFieldException, IllegalAccessException {
        Field salary = user.getClass().getDeclaredField("salary");
        salary.setAccessible(true);
        double newSalary = 0.0;

        System.out.println("Present salary for " + user.getLogin() + ": " + salary.getDouble(user));
        System.out.println();
        System.out.println("Put new value for field 'salary':");

        try {
            if (SCANNER.hasNextDouble())
                newSalary = SCANNER.nextDouble();
        } catch (InputMismatchException e) {
            throw new InputMismatchException("Bad choice");
        }
        salary.setDouble(user, newSalary);

        System.out.println("New salary: " + salary.getDouble(user));
    }
}
